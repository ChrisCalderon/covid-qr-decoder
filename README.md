# COVID-QR-decoder

Decodes the information contained in the QR code of a California digital vaccine record.

If you live in California and have recieved a COVID-19 vaccination, then you can (theoretically) access a digital record of your vaccinations via the CA Dept. of Public Health [website](https://myvaccinerecord.cdph.ca.gov/). This record contains a QR code, and the specification for the data encoded in that QR code is found [here](https://spec.smarthealth.cards/). This module can scan a QR code vaccine record and decode the data within, via the SmartHealthCard class. At the moment it does not verify the data but that should be done soon. This module also provides a script that takes a file name as an argument and prints out the data. To use the script, enter this command: python -m covid_qr_decoder your_qr_filename_here

This module depends on Pillow and PyZBar for image processing. PyZBar depends on ZBar being installed on your system. For macOS, you can install it via MacPorts. For Linux it should be available through your distrobution's package manager.
