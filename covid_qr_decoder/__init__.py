# COVID-QR-decoder
# Copyright (C) 2021  Christian Calderon

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# You can contact the author via email at calderon DOT christian73 AT gmail DOT com.
"""This module parses the data found in a COVID vaccine record QR code."""
from pyzbar.pyzbar import decode as qr_decode, ZBarSymbol
from PIL import Image
from binascii import a2b_base64
from zlib import decompress

QR_DATA_PREFIX = b'shc:/'
QR_DATA_PREFIX_LEN = 5


def is_even(n: int) -> bool:
    """Tells if a number is even.

    This function is dedicated to the Programmer ɴᴜʟʟposting Facebook group.
    """
    return not n&1


def numeric_decode(buffer: bytes) -> bytearray:
    """Undoes the quirky encoding used on JWS strings before QR encoding.

    The output is a bytearray containing base64url encoded sections, 
    concattenated with a '.' character inbetween them.

    The length of the output is exactly half the length of the input.

    If the input buffer is invalid, a ValueError is raised.
    """

    # The encoding process takes each character c, computes ord(c) - 45,
    # and writes the result as two base-10 digits. 45 is the value of ord('_')
    # which is the smallest value used in the base64url encoding.
    # 
    # Decoding this means taking two characters at a time, c1 and c2,
    # converting them into an int, and adding 45.
    #
    # Since ord('0') equals 48, the int value n represented by c1c2 is
    # n = 10*(ord(c1) - 48) + (ord(c2) - 48)
    # The original character c is thus chr(n + 45)
    #
    # Since I'm working with bytearray, each item in the array is already an
    # int so I can avoid calling ord and chr. Thus the conversion simplifies:
    # c = 10*c1 - 10*48 + c2 - 48 + 45
    #   = 10*c1 + c2 - 483

    l = len(buffer)
    if buffer.isdigit() and is_even(l):
        return bytearray(
            [10*buffer[i] + buffer[i+1] - 483 for i in range(0, l, 2)]
        )
    else:
        raise ValueError('Input must be even number of decimal characters.')


def make_translation_table(old_chars: bytes, new_chars: bytes) -> bytearray:
    """Creates a lookup table to quickly replace characters in bytearray."""
    result = bytearray(range(256))
    for old_char, new_char in zip(old_chars, new_chars):
        result[old_char] = new_char
    return result


def inplace_translate(buffer: bytearray, table: bytearray) -> None:
    """Translate the contents of the buffer "inplace" using the table."""
    for i in range(len(buffer)):
        buffer[i] = table[buffer[i]]


def b64url_to_b64(buffer: bytearray,
                  table: bytearray=make_translation_table(b'-_', b'+/')) -> None:
    """Converts a base64-url buffer into a base64 buffer, "inplace".

    The table argument is used to translate the buffer. By default it should
    work with the encoding used with digital COVID vaccine records.
    """
    l = len(buffer)
    l_m = l % 4

    if l_m == 1:
        raise ValueError('length of buffer cannot be 1 more than a multiple of 4.')

    inplace_translate(buffer, table)

    buffer.extend((b'', None, b'==', b'=')[l_m])

        
class SmartHealthCard:
    def __init__(self, qr_filename: str):
        image = Image.open(qr_filename)
        data = qr_decode(image, symbols=[ZBarSymbol.QRCODE])

        if len(data) != 1:
            raise ValueError('file has too many QR codes.')

        if data[0].data[:QR_DATA_PREFIX_LEN] != QR_DATA_PREFIX:
            raise ValueError(f'unexpected data format: {data[0].data.decode()}') 

        shc_bytes = data[0].data[QR_DATA_PREFIX_LEN:]
        self.compact_jws = numeric_decode(shc_bytes)
        jws_header, jws_payload, jws_signature = self.compact_jws.split(b'.')
        for data in (jws_header, jws_payload, jws_signature):
            b64url_to_b64(data)

        self.header = a2b_base64(jws_header)
        self.payload = decompress(a2b_base64(jws_payload), -15)
        self.signature = a2b_base64(jws_signature)
