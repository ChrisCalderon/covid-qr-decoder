#!/usr/bin/env/python
# COVID-QR-decoder
# Copyright (C) 2021  Christian Calderon

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# You can contact the author via email at calderon DOT christian73 AT gmail DOT com.
from covid_qr_decoder import SmartHealthCard
import argparse
import sys
import os

EXIT_SUCCESS = 0
EXIT_FAILURE = 1


def main():
    parser = argparse.ArgumentParser(
        description="Decodes the data found in a digital COVID vaccine record QR code."
    )
    parser.add_argument('qr_filename', help='The name of the file containing the text from the QR code.')
    args = parser.parse_args()
    qr_filename = args.qr_filename
    
    if not os.path.isfile(qr_filename):
        print(f'The path provided ({qr_filename}) is not a file!', file=sys.stderr)
        sys.exit(EXIT_FAILURE)

    smart_health_card = SmartHealthCard(qr_filename)
    print('Compact JWS representation:\n', smart_health_card.compact_jws.decode())
    print()
    print('Header:\n', smart_health_card.header.decode())
    print()
    print('Payload:\n', smart_health_card.payload.decode())
    print()
    sys.exit(EXIT_SUCCESS)


if __name__ == '__main__':
    main()
